# bootstrap_python

This role installs python3 on using the *raw* module, as it is a requirement for
ansible to run.
It currently supports Archlinux-likes, RHEL-likes and Debian-likes.

# Example Playbook

```yaml
---
- name: Bootstrap
  hosts: "all"
  roles:
    - role: bootstrap_python
```

# License

GPLv3

# Author information

Wallun <wallun AT disroot DOT org>
